<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 */

namespace els\test;

class Fileman{

    private $files = [];
    private $filepaths = [];
    private $path;
    private $db;
    public $message;



    public function __construct($path, $recache=true, $db)
    {
        $this->path = $path;
        $this->db = $db;
        $this->read(!$recache&&self::checkBase($this->db));
    }

    private  function checkBase(){
        $res = $this->db->query("SELECT id FROM fileman");
        return count($res->fetch_all())!=0;
    }

    private function read($cache=false){
        if($cache){
            $this->readBase();
            $this->message = "Used Cache";
        }
        else{
            $this->message = "Used FileScan";
            $this->readFileSystem();
            $this->collectInfo();
            $this->saveData();
        }
    }

    private function readBase(){
        $res = $this->db->query("SELECT name, type, size, dir, changed FROM fileman");
        $this->files = mysqli_fetch_all($res,MYSQLI_ASSOC);
    }

    public function viewFiles(){
        echo "<table class='table'>";
        echo '<thead class="thead-light"><tr><th scope="col">Name</th><th scope="col">Size</th><th scope="col">Extension</th><th scope="col">Changed</th></tr></thead>';
        foreach ($this->files as $file) {
            self::renderItem($file);
        }
        echo "</table>";
    }

    private function readFileSystem(){
        $this->filepaths = glob($this->path."/*", GLOB_ONLYDIR);
        $this->filepaths = array_unique(array_merge($this->filepaths, glob($this->path."/*")) );
    }

    private function collectInfo(){
        foreach ($this->filepaths as $filepath){
            $this->files[]=is_dir($filepath)?
                [
                    'name' => basename($filepath),
                    'type' => '',
                    'size' => 'DIR',
                    'changed' => date("Y-m-d H:i:s", filectime($filepath)),
                    'dir' => true
                ]:
                [
                    'name' => pathinfo($filepath)['filename'],
                    'type' => pathinfo($filepath)['extension'],
                    'size' => filesize($filepath)."b",
                    'changed' => date("Y-m-d H:i:s", filectime($filepath)),
                    'dir' => false
                ];
        }
    }

    private static function renderItem($item){
        if($item['dir']){
            echo "<tr><td><i class=\"far fa-folder\"></i> ".$item['name']."</td><td><small class='text-muted'>".$item['size'].'</small></td><td> <span class="badge badge-light">'. $item['type']."</span></td><td> <small class='text-muted'>". $item['changed']."</small></td></tr>";
        }
        else{
            echo "<tr><td><i class=\"far fa-file\"></i> ".$item['name']."</td><td><small class='text-muted'>".$item['size'].'</small></td><td> <span class="badge badge-light">'. $item['type']."</span></td><td> <small class='text-muted'>". $item['changed']."</small></td></tr>";
        }
    }

    private function saveData()
    {
        $this->db->query("TRUNCATE fileman");
        $stmt = $this->db->prepare("INSERT INTO fileman(name, type, size, dir, changed) VALUES (?,?,?,?,?)");

        foreach ($this->files as $item){
            $stmt->bind_param("ssiis", $item['name'],$item['type'],$item['size'],$item['dir'],$item['changed']);
            $stmt->execute();
        }
    }


}

<?php

require "./Fileman/Fileman.php";
require  "/conf/conf.php";


$rebase= false;

$mysqli = new mysqli($db['host'], $db['user'], $db['pass'], $db['db']);
if(isset($_POST['rebase']))
   $rebase = filter_var($_POST['rebase'], FILTER_SANITIZE_STRING);
$fileman = new els\test\Fileman($root, $rebase==="rebase", $mysqli);
?>

<html>
    <head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css"  crossorigin="anonymous">
    </head>
    <body>
    <div class="container">
        <div class="col-lg-12 py-5 ">
            <div class="alert alert-light" role="alert">
               <?=$fileman->message;?>
            </div>
            <h1>Directory</h1>
            <div class="container py-5">
                <?php $fileman->viewFiles();?>
                    <form  method="Post">
                        <input type="hidden" name="rebase" value="rebase">
                    <button type="submit" class="btn btn-dark">Refresh</button>
                    </form>
            </div>
        </div>
    </div>
    </body>
</html>

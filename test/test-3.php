<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 10.12.18
 * Time: 22:47
 */


$format = 'd.m.Y';
$date_correct = '24.07.1985';
$date_incorrect = '42.07.1985';


function validDate($format, $date){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format)==$date;

}


var_dump(validDate($format, $date_correct)); //True
var_dump(validDate($format, $date_incorrect)); // False


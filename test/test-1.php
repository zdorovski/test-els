<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 10.12.18
 * Time: 21:56
 */

trait saveTrait{
    public function save(){
        return $this;
    }
}

class Building implements ArrayAccess  {
    use saveTrait;

    public $name;
    public $flats;

      public function offsetSet($offset, $value) {
        $this->$offset = $value;

    }

    public function offsetExists($offset) {
        return isset($this->$offset);
    }

    public function offsetUnset($offset) {
        unset($this->$offset);
    }

    public function offsetGet($offset) {
        return isset($this->$offset) ? $this->$offset : null;
    }

}


class BuildingObject extends ArrayObject{

    use saveTrait;
    public function __get($key)
    {
        return  $this[$key];
    }

    public function __set($name, $value)
    {
        $this[$name] = $value;
    }


}

$obj = new Building();
$obj['named'] = 'Main tower';
$obj['flats'] =  100;
$obj->save();


$obj_obj = new BuildingObject();
$obj_obj['name'] = "Second Tower";
$obj_obj->save();

